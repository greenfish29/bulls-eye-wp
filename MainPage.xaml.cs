﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using BullsEye.Resources;

namespace BullsEye
{
    public partial class MainPage : PhoneApplicationPage
    {
        private const string _keyRound = "BullsEye.Round";
        private const string _keyScore = "BullsEye.Score";
        private const string _keyTarget = "BullsEye.Target";
        private const string _keySliderValue = "BullsEye.SliderValue";

        public int Round { get; private set; }
        public int Score { get; private set; }
        public int Target { get; private set; }

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            DataContext = this;
            StartNewGame();
        }

        private void btnHitMe_Click(object sender, RoutedEventArgs e)
        {
            int value = (int)Math.Round(sldSlider.Value);
            int points = 100 - Math.Abs(Target - value);

            string title;
            if (points == 100)
            {
                title = AppResources.titlePerfect;
                points += 100;
            }
            else if (points > 95)
            {
                if (points == 99) { points += 50; }
                title = AppResources.titleAlmost;
            }
            else if (points > 90)
            { 
                title = AppResources.titleGood;
            }
            else
            {   
                title=AppResources.titleNotClose;
            }

            string text = String.Format(AppResources.ScoredMessage, points);
            MessageBox.Show(text, title, MessageBoxButton.OK);

            Score += points;
            StartNewRound();
            UpdateLabels();
        }

        private void StartNewRound()
        {
            Round++;
            sldSlider.Value = 50;

            Random rand = new Random(DateTime.Now.Millisecond);
            Target = rand.Next(1, 101);
        }

        private void StartNewGame()
        {
            Round = 0;
            Score = 0;

            StartNewRound();
            UpdateLabels();
        }

        private void UpdateLabels()
        {
            tbRound.Text = Round.ToString();
            tbScore.Text = Score.ToString();
            tbTarget.Text = Target.ToString();
        }

        private void btnStartOver_Click(object sender, RoutedEventArgs e)
        {
            StartNewGame();
            UpdateLabels();
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (this.State.ContainsKey(_keyRound))
            {
                tbRound.Text = this.State[_keyRound].ToString();
                tbScore.Text = this.State[_keyScore].ToString();
                tbTarget.Text = this.State[_keyTarget].ToString();
                sldSlider.Value = (double)this.State[_keySliderValue];
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // If was windows button pressed or about page navigated.
            if (e.NavigationMode == System.Windows.Navigation.NavigationMode.New)
            {
                // Save data to page state.
                State[_keyRound] = Round;
                State[_keyScore] = Score;
                State[_keyTarget] = Target;
                State[_keySliderValue] = sldSlider.Value;
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show(AppResources.mbExitText, AppResources.mbExitTitle, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                base.OnBackKeyPress(e);
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}